# LoRa RP Production

## Overview

### Owners & Administrators

| Title       | Details                                                                     |
|-------------|-----------------------------------------------------------------------------|
| **Section** | [BE-CEM-EPR](https://phonebook.cern.ch/search?q=BE-CEM-EPR)                 |
| **E-group** | -                                                                           |
| **People**  | [Alessandro Zimmaro](https://phonebook.cern.ch/search?q=Alessandro+Zimmaro) |

### Kafka Topics

| Environment    | Topic Name                                                                                                                    |
|----------------|-------------------------------------------------------------------------------------------------------------------------------|
| **Production** | [lora-rp-production](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-rp-production)                 |
| **Production** | [lora-rp-production-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-rp-production-decoded) |
| **QA**         | [lora-rp-production](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-rp-production)                 |
| **QA**         | [lora-rp-production-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-rp-production-decoded) |

### Configuration

| Title                        | Details                                                                                                          |
|------------------------------|------------------------------------------------------------------------------------------------------------------|
| **Application Name**         | cern-rp-wmon-production                                                                                          |
| **Configuration Repository** | [app-configs/lora-rp-production](https://www.gitlab.cern.ch/nile/streams/app-configs/lora-rp-production) |

### Technologies

- LoRa
