FROM gitlab-registry.cern.ch/nile/streams/base-app-image:v2.1.0

COPY target/*jar-with-dependencies.jar /kafka/lora-rp-production-app.jar

COPY src/main/resources/log4j.properties /kafka/log4j.properties
