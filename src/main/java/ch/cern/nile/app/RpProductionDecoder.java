package ch.cern.nile.app;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.cern.nile.common.exceptions.DecodingException;

/**
 * Utility class for decoding RP production LoRaWAN payloads. It handles JSON parsing,
 * extracts data, manages data loss scenarios, and normalizes counts.
 * <p>
 * TODO(#NILE-1095): Convert this into Kaitai. If not possible, refactor to improve readability make PMD happy.
 */
@SuppressWarnings({"checkstyle:LineLength", "PMD.AvoidLiteralsInIfCondition", "PMD.AvoidDuplicateLiterals",
        "PMD.GodClass", "PMD.TooManyMethods", "PMD.CyclomaticComplexity", "PMD.CognitiveComplexity", "PMD.NcssCount",
        "PMD.SwitchDensity", "PMD.OnlyOneReturn"})
public final class RpProductionDecoder {

    private static final Logger LOGGER = LoggerFactory.getLogger(RpProductionDecoder.class.getName());
    private static final double VOLTAGE_CONSTANT = 0.000_805_664_062_5;
    private static final double VOLTAGE_CONSTANT_2 = 0.001_853_027_343_75;
    private static final int TRANSMISSION_PERIOD = 3600; // 1 hour
    private static final float TRANSMISSION_MARGIN = 0.15f;
    // 540s Due to different factors LoRa transmission timing can vary with some tolerance.

    private RpProductionDecoder() {
    }

    /**
     * Decodes payload and adds properties to the returning  objects.
     *
     * @param message             - incoming message
     * @param lastDeviceTimestamp - devices' last timestamp
     * @return - Map of properties of the decoded payload
     */
    public static Collection<Map<String, Object>> decode(final JsonObject message, final Long lastDeviceTimestamp)
            throws IOException, ParseException {
        final String payloadRaw = message.get("data").getAsString();
        final byte[] payloadDecoded = Base64.getDecoder().decode(payloadRaw);
        final DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(payloadDecoded));

        final long currTimestamp = findTimestamp(message);

        Collection<Map<String, Object>> decodedPayload = Collections.emptyList();

        if (inputStream.available() > 0) {
            final Map<String, Object> payload = decodePayload(inputStream);
            final int packageNum = (int) payload.get("package_num");
            final long timestampDiff = calculateTimestampDiff(lastDeviceTimestamp, currTimestamp);

            decodedPayload = handlePayloadBasedOnTimeDiff(message, currTimestamp, payload, packageNum, timestampDiff);
        }
        return decodedPayload;
    }

    private static long calculateTimestampDiff(final Long lastDeviceTimestamp, final long currTimestamp) {
        return Math.abs(currTimestamp - (lastDeviceTimestamp != null ? lastDeviceTimestamp : currTimestamp));
    }

    private static Collection<Map<String, Object>> handlePayloadBasedOnTimeDiff(final JsonObject message,
                                                                                final long currTimestamp,
                                                                                final Map<String, Object> payload,
                                                                                final int packageNum,
                                                                                final long timestampDiff) {
        final Collection<Map<String, Object>> decodedPayload = new ArrayList<>();

        if (isDataLossSignificant(timestampDiff, 2)) {
            // 2h + 540s
            if (packageNum >= 3) {
                decodedPayload.add(getSpecificPayloadData(0, payload, message, currTimestamp));
                decodedPayload.add(getSpecificPayloadData(1, payload, message, currTimestamp));
                decodedPayload.add(getSpecificPayloadData(2, payload, message, currTimestamp));
            } else if (packageNum == 2) {
                // Account for cases when the device was having problems for more than 2h15min
                decodedPayload.add(getSpecificPayloadData(0, payload, message, currTimestamp));
                decodedPayload.add(getSpecificPayloadData(1, payload, message, currTimestamp));
            } else if (packageNum == 1) {
                // Account for cases when the device was having problems for more than 2h15min
                decodedPayload.add(getSpecificPayloadData(0, payload, message, currTimestamp));
            } else {
                throw new DecodingException(
                        String.format("Message %s had data loss of %d hours but packageNum was %d", message, 2,
                                packageNum));
            }
        } else if (isDataLossSignificant(timestampDiff, 1)) {
            // 1h + 540s
            if (packageNum >= 2) {
                decodedPayload.add(getSpecificPayloadData(0, payload, message, currTimestamp));
                decodedPayload.add(getSpecificPayloadData(1, payload, message, currTimestamp));
            } else {
                throw new DecodingException(
                        String.format("Message %s had data loss of %d hour but packageNum was %d", message, 1,
                                packageNum));
            }
        } else {
            if (packageNum >= 1) {
                decodedPayload.add(getSpecificPayloadData(0, payload, message, currTimestamp));
            } else {
                throw new DecodingException(
                        String.format("Message %s is without data loss but packageNum was %d", message, packageNum));
            }
        }
        return decodedPayload;
    }

    private static boolean isDataLossSignificant(final long timestampDiff, final int hours) {
        final float threshold = (TRANSMISSION_PERIOD + TRANSMISSION_MARGIN * TRANSMISSION_PERIOD) * hours * 1000;
        return timestampDiff > threshold;
    }

    private static long findTimestamp(final JsonObject message) {
        final JsonArray metadata = message.get("rxInfo").getAsJsonArray();
        boolean timestampFound = false;
        long timestamp = 0;
        for (int i = 0; i < metadata.size(); i++) {
            final JsonObject entry = metadata.get(i).getAsJsonObject();
            if (entry.get("time") != null) {
                timestamp = Instant.parse(entry.get("time").getAsString()).toEpochMilli();
                timestampFound = true;
                break;
            }
        }
        if (!timestampFound) {
            throw new DecodingException(
                    String.format("Current timestamp was not present in the message: %s; Dropping the message",
                            metadata));
        }
        return timestamp;
    }

    private static Map<String, Object> getSpecificPayloadData(final int dataLossCase,
                                                              final Map<String, Object> payloadData,
                                                              final JsonObject message, final long currTimestamp) {
        final Map<String, Object> decoded = new HashMap<>();
        final int alarm = (int) payloadData.get("alarm");

        decoded.put("dev_ID", payloadData.get("dev_ID"));
        decoded.put("package_num", payloadData.get("package_num"));

        switch (dataLossCase) {
            case 0:
                decoded.put("redundant_alg", 0);
                decoded.put("package_num", payloadData.get("package_num"));
                decoded.put("alarm", payloadData.get("alarm"));
                decoded.put("alarm_counts", payloadData.get("alarm_counts"));
                decoded.put("counts", payloadData.get("counts"));
                decoded.put("counts_normalized",
                        normalizeCounts((int) payloadData.get("counts"), (int) payloadData.get("checkingTime")));
                decoded.put("checking_time", payloadData.get("checkingTime"));
                decoded.put("shock_counts", payloadData.get("shockCounts"));
                decoded.put("timestamp", currTimestamp);
                decoded.put("exwdtc", payloadData.get("exwdtc"));

                break;
            case 1:
                decoded.put("redundant_alg", 1);
                decoded.put("package_num", (int) payloadData.get("package_num") - 1);
                decoded.put("alarm", 0);
                decoded.put("counts", payloadData.get("counts1hAgo"));
                decoded.put("counts_normalized", normalizeCounts((int) payloadData.get("counts1hAgo"),
                        (int) payloadData.get("checkingTime1hAgo")));
                decoded.put("checking_time", payloadData.get("checkingTime1hAgo"));
                decoded.put("shock_counts", 0);
                decoded.put("exwdtc", (int) payloadData.get("exwdtc") - 60);

                if (alarm == 0 || alarm == 1) {
                    decoded.put("timestamp", currTimestamp - (TRANSMISSION_PERIOD * 1000L));
                } else {
                    switch (alarm) {
                        case 2:
                        case 5:
                            // Insert 1 hour ago because alarm has not been triggered or alarm has been triggered in last period.
                            decoded.put("timestamp", currTimestamp - (TRANSMISSION_PERIOD * 1000L));
                            decoded.put("exwdtc", (int) payloadData.get("exwdtc") - 60);
                            decoded.put("alarm", 2);
                            break;
                        case 3:
                            // Insert 20 minutes ago because alarm has been triggered now in the first period of checking.
                            decoded.put("timestamp", currTimestamp - (TRANSMISSION_PERIOD / 3 * 1000L));
                            decoded.put("exwdtc", (int) payloadData.get("exwdtc") - 20);
                            decoded.put("alarm", 2);
                            break;
                        case 4:
                            // Insert 40 minutes ago because alarm has been triggered now in the first period of checking.
                            decoded.put("timestamp", currTimestamp - (long) TRANSMISSION_PERIOD / 3 * 2 * 1000);
                            decoded.put("exwdtc", (int) payloadData.get("exwdtc") - 40);
                            decoded.put("alarm", 2);
                            break;
                        default:
                            // We should abort this message insertion here because alarm should never be higher than 4
                            decoded.clear();
                            return decoded;
                    }
                }
                break;
            case 2:
                decoded.put("redundant_alg", 2);
                decoded.put("package_num", (int) payloadData.get("package_num") - 2);
                decoded.put("alarm", 0);
                decoded.put("alarm_counts", 0);
                decoded.put("counts", payloadData.get("counts2hAgo"));
                decoded.put("counts_normalized", normalizeCounts((int) payloadData.get("counts2hAgo"),
                        (int) payloadData.get("checkingTime2hAgo")));
                decoded.put("checking_time", payloadData.get("checkingTime2hAgo"));
                decoded.put("shock_counts", 0);
                decoded.put("exwdtc", (int) payloadData.get("exwdtc") - 120);
                if (alarm == 0 || alarm == 1) {
                    decoded.put("timestamp", currTimestamp - (2L * TRANSMISSION_PERIOD * 1000));
                } else {
                    switch (alarm) {
                        case 2:
                        case 5:
                            // Insert 2 hour ago because alarm has not been triggered or alarm has been triggered in last period.
                            decoded.put("timestamp", currTimestamp - (2L * TRANSMISSION_PERIOD * 1000));
                            decoded.put("exwdtc", (int) payloadData.get("exwdtc") - 120);
                            decoded.put("alarm", 2);
                            break;
                        case 3:
                            // Insert 80 minutes ago because alarm has been triggered now in the first period of checking.
                            decoded.put("timestamp",
                                    currTimestamp - (2L * TRANSMISSION_PERIOD - TRANSMISSION_PERIOD / 3 * 2) * 1000);
                            decoded.put("exwdtc", (int) payloadData.get("exwdtc") - 80);
                            decoded.put("alarm", 2);
                            break;
                        case 4:
                            // Insert 100 minutes ago because alarm has been triggered now in the first period of checking.
                            decoded.put("timestamp",
                                    currTimestamp - (2L * TRANSMISSION_PERIOD - TRANSMISSION_PERIOD / 3) * 1000);
                            decoded.put("exwdtc", (int) payloadData.get("exwdtc") - 100);
                            decoded.put("alarm", 2);
                            break;
                        default:
                            // We should abort this message insertion here because alarm should never be higher than 4
                            decoded.clear();
                            return decoded;
                    }
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + dataLossCase);
        }
        decoded.put("temperature", payloadData.get("temperature"));
        decoded.put("mon3v3", payloadData.get("mon3v3"));
        decoded.put("mon3v3_voltage", payloadData.get("mon3v3_voltage"));
        decoded.put("mon5", payloadData.get("mon5"));
        decoded.put("mon5_voltage", payloadData.get("mon5_voltage"));
        decoded.put("monVin", payloadData.get("monVin"));
        decoded.put("monVin_voltage", payloadData.get("monVin_voltage"));

        appendMetadata(message, decoded);
        return decoded;
    }

    private static Map<String, Object> decodePayload(final DataInputStream inputStream) throws IOException {
        final Map<String, Object> output = new HashMap<>();

        // Common bytes
        final int devId = inputStream.readUnsignedByte();
        final int pNum = inputStream.readUnsignedShort();

        //30 bytes of sensor payload data
        final int alarm = inputStream.readUnsignedByte();
        final int alarmCounts = inputStream.readInt();
        final int counts = inputStream.readInt();
        final int counts1hAgo = inputStream.readInt();
        final int counts2hAgo = inputStream.readInt();
        final int checkingTime = inputStream.readUnsignedShort();
        final int checkingTime1hAgo = inputStream.readUnsignedShort();
        final int checkingTime2hAgo = inputStream.readUnsignedShort();
        final int shockCounts = inputStream.readInt();
        final int temperature = inputStream.readUnsignedShort();

        final int result = inputStream.skipBytes(1); // unused

        if (result != 1 && LOGGER.isWarnEnabled()) {
            LOGGER.warn("Could not skip 1 byte. Skipping 1 byte returned: " + result);
        }

        // Common bytes
        final int mon3v3 = inputStream.readUnsignedShort();
        final int mon5 = inputStream.readUnsignedShort();
        final int monVin = inputStream.readUnsignedShort();
        final int exwdtc = inputStream.readUnsignedShort();
        final double mon3V3Voltage = mon3v3 * VOLTAGE_CONSTANT;
        final double mon5Voltage = mon5 * VOLTAGE_CONSTANT;
        final double monVinVoltage = monVin * VOLTAGE_CONSTANT_2;

        output.put("dev_ID", devId);
        output.put("package_num", pNum);
        output.put("alarm", alarm);
        output.put("alarm_counts", alarmCounts);
        output.put("counts", counts);
        output.put("counts1hAgo", counts1hAgo);
        output.put("counts2hAgo", counts2hAgo);
        output.put("checkingTime", checkingTime);
        output.put("checkingTime1hAgo", checkingTime1hAgo);
        output.put("checkingTime2hAgo", checkingTime2hAgo);
        output.put("shockCounts", shockCounts);
        output.put("temperature", temperature);

        // Voltages and extWDT counter
        output.put("mon3v3", mon3v3);
        output.put("mon3v3_voltage", mon3V3Voltage);
        output.put("mon5", mon5);
        output.put("mon5_voltage", mon5Voltage);
        output.put("monVin", monVin);
        output.put("monVin_voltage", monVinVoltage);
        output.put("exwdtc", exwdtc);

        return output;
    }

    private static double normalizeCounts(final int counts, final int checkingTime) {
        return (checkingTime == 0) ? 0.0 : (float) counts / checkingTime * 3600;
    }

    private static void appendMetadata(final JsonObject message, final Map<String, Object> obj) {
        // Add device name
        addJsonPropertyIfExists(message, "deviceName", "device_name", obj);

        // Add port as property
        addJsonPropertyIfExists(message, "fPort", "fPort", obj);

        // Add RSSI(s) and SNR(s) as properties
        if (message.has("rxInfo")) {
            final JsonArray metadata = message.get("rxInfo").getAsJsonArray();
            int index = 0;
            for (final JsonElement element : metadata) {
                final JsonObject entry = element.getAsJsonObject();
                addGatewayProperty(entry, "rssi", "rssi_gw_%d", index, obj);
                addGatewayProperty(entry, "loRaSNR", "snr_gw_%d", index, obj);
                index++;
            }
        } else if (LOGGER.isWarnEnabled()) {
            LOGGER.warn("Message does not contain rxInfo. Not appending RSSI and SNR values.");
        }
    }

    private static void addJsonPropertyIfExists(final JsonObject jsonObject, final String propertyName,
                                                final String formattedName, final Map<String, Object> obj) {
        if (jsonObject.has(propertyName)) {
            obj.put(formattedName, jsonObject.get(propertyName).getAsString());
        } else if (LOGGER.isWarnEnabled()) {
            LOGGER.warn("Message does not contain {}. Not appending value.", propertyName);
        }
    }

    private static void addGatewayProperty(final JsonObject entry, final String propertyName,
                                           final String formattedName, final int index, final Map<String, Object> obj) {
        if (entry.has(propertyName)) {
            obj.put(String.format(formattedName, index), entry.get(propertyName).getAsInt());
        } else if (LOGGER.isWarnEnabled()) {
            LOGGER.warn("Gateway {} does not contain {}. Not appending value.", index, propertyName);
        }
    }

}
