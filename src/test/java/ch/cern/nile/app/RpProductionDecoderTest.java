package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import com.google.gson.JsonObject;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.test.utils.TestUtils;

@SuppressWarnings("PMD.TooManyMethods")
class RpProductionDecoderTest {

    private static final JsonObject MESSAGE_0 = TestUtils.loadRecordAsJson("data/message.json");

    private static final JsonObject MESSAGE_NO_RSSI_NO_SNR =
            TestUtils.loadRecordAsJson("data/message_no_rssi_no_snr.json");

    private static final JsonObject MESSAGE_NO_TIME = TestUtils.loadRecordAsJson("data/message_no_time.json");

    // packet #1 => 1h loss; alarm = 0; counts_alarm = 13; count = 5; counts1h = 10; counts2h = 20;
    private static final JsonObject MESSAGE_1 = TestUtils.loadRecordAsJson("data/message_1.json");

    // packet #2 => 2h loss; alarm = 0; counts_alarm = 13; count = 5; counts1h = 10; counts2h = 20;
    private static final JsonObject MESSAGE_2 = TestUtils.loadRecordAsJson("data/message_2.json");

    // 1h loss pkt number 4 exwdtc - 240
    private static final JsonObject MESSAGE_3 = TestUtils.loadRecordAsJson("data/message_3.json");

    // 2h loss pkt number 4 exwdtc - 240
    private static final JsonObject MESSAGE_4 = TestUtils.loadRecordAsJson("data/message_4.json");

    // 2h loss pkt number 4 exwdtc - 240 ALARM = 1
    private static final JsonObject MESSAGE_5 = TestUtils.loadRecordAsJson("data/message_5.json");

    // 2h loss pkt number 4 exwdtc - 240 ALARM = 2
    private static final JsonObject MESSAGE_6 = TestUtils.loadRecordAsJson("data/message_6.json");

    // 2h loss pkt number 4 exwdtc - 240 ALARM = 3
    private static final JsonObject MESSAGE_7 = TestUtils.loadRecordAsJson("data/message_7.json");

    // 2h loss pkt number 4 exwdtc - 240 ALARM = 4
    private static final JsonObject MESSAGE_8 = TestUtils.loadRecordAsJson("data/message_8.json");

    // 2h loss pkt number 4 exwdtc - 240 ALARM = 5
    private static final JsonObject MESSAGE_9 = TestUtils.loadRecordAsJson("data/message_9.json");

    @Test
    void givenMessage0_whenDecoding_thenDecodesCorrectly() throws IOException, ParseException {
        final Optional<Map<String, Object>> decoded = RpProductionDecoder.decode(MESSAGE_0, null).stream().findFirst();

        assertTrue(decoded.isPresent());
        assertEquals(2572, decoded.get().get("monVin"));
        assertEquals(4.765_986_328_125, decoded.get().get("monVin_voltage"));
        assertEquals(5, decoded.get().get("snr_gw_0"));
        assertEquals(-2, decoded.get().get("snr_gw_1"));
        assertEquals(3.299_194_335_937_5, decoded.get().get("mon3v3_voltage"));
        assertEquals(2, decoded.get().get("exwdtc"));
        assertEquals("8", decoded.get().get("fPort"));
        assertEquals(50, decoded.get().get("counts"));
        assertEquals(4095, decoded.get().get("mon3v3"));
        assertEquals(221, decoded.get().get("dev_ID"));
        assertEquals(1.429_248_046_875, decoded.get().get("mon5_voltage"));
        assertEquals("rp-wmon-44", decoded.get().get("device_name"));
        assertEquals(16, decoded.get().get("shock_counts"));
        assertEquals(1, decoded.get().get("alarm"));
        assertEquals(290, decoded.get().get("temperature"));
        assertEquals(50, decoded.get().get("alarm_counts"));
        assertEquals(1774, decoded.get().get("mon5"));
        assertEquals(3600, decoded.get().get("checking_time"));
        assertEquals(1, decoded.get().get("package_num"));
        assertEquals(1_605_717_938_463L, decoded.get().get("timestamp"));
        assertEquals(-108, decoded.get().get("rssi_gw_0"));
        assertEquals(-107, decoded.get().get("rssi_gw_1"));
        assertEquals(5, decoded.get().get("snr_gw_0"));
        assertEquals(-2, decoded.get().get("snr_gw_1"));
    }

    @Test
    void givenMessageNoRssiNoSnr_whenDecoding_thenDecodesCorrectly() throws IOException, ParseException {
        final Optional<Map<String, Object>> decoded =
                RpProductionDecoder.decode(MESSAGE_NO_RSSI_NO_SNR, null).stream().findFirst();

        assertTrue(decoded.isPresent());
        assertNull(decoded.get().get("rssi_gw_0"));
        assertNull(decoded.get().get("rssi_gw_1"));
        assertNull(decoded.get().get("snr_gw_0"));
        assertNull(decoded.get().get("snr_gw_1"));
    }

    @Test
    void givenMessageNoTime_whenDecoding_thenThrowsDecodingException() {
        final DecodingException cause = assertThrows(DecodingException.class,
                () -> RpProductionDecoder.decode(MESSAGE_NO_TIME, 1_610_615_001_000L));
        assertTrue(cause.getMessage().contains("Current timestamp was not present in the message"));
    }

    @Test
    void given1hRedundantMessage_whenDecoding_thenThrowsDecodingException() {
        final DecodingException cause =
                assertThrows(DecodingException.class, () -> RpProductionDecoder.decode(MESSAGE_1, 1_610_611_803_000L));
        assertTrue(cause.getMessage().contains("had data loss of 1 hour but packageNum was 1"));
    }

    @Test
    void given1hRedundantMessage_whenDecoding_thenDecodesCorrectly() throws IOException, ParseException {
        final Collection<Map<String, Object>> decoded = RpProductionDecoder.decode(MESSAGE_3, 1_610_615_001_000L);
        assertEquals(2, decoded.size());
    }

    @Test
    void given2hRedundantMessageWithPackageSize2_whenDecoding_ThenDecodesCorrectly()
            throws IOException, ParseException {
        final Collection<Map<String, Object>> decoded = RpProductionDecoder.decode(MESSAGE_2, 1_610_604_603_000L);
        assertEquals(2, decoded.size());
    }

    @Test
    void given2hRedundantMessageWithPackageSize3_whenDecoding_thenDecodesCorrectly()
            throws IOException, ParseException {
        final Collection<Map<String, Object>> decode = RpProductionDecoder.decode(MESSAGE_4, 1_610_607_801_000L);
        assertEquals(3, decode.size());
    }

    @Test
    void given2hRedundantMessageWithAlarm_whenDecoding_thenDecodesCorrectly() throws IOException, ParseException {
        final Collection<Map<String, Object>> decoded = RpProductionDecoder.decode(MESSAGE_5, 1_610_607_801_000L);
        assertEquals(3, decoded.size());
    }

    @Test
    void given2hRedundantMessageWithAlarm2_whenDecoding_thenDecodesCorrectly() throws IOException, ParseException {
        final Collection<Map<String, Object>> decoded = RpProductionDecoder.decode(MESSAGE_6, 1_610_607_801_000L);
        assertEquals(3, decoded.size());

    }

    @Test
    void given2hRedundantMessageWithAlarm3_whenDecoding_thenDecodesCorrectly() throws IOException, ParseException {
        final Collection<Map<String, Object>> decode = RpProductionDecoder.decode(MESSAGE_7, 1_610_607_801_000L);
        assertEquals(3, decode.size());
    }

    @Test
    void given2hRedundantMessageWithAlarm4_whenDecoding_thenDecodesCorrectly() throws IOException, ParseException {
        final Collection<Map<String, Object>> decode = RpProductionDecoder.decode(MESSAGE_8, 1_610_607_801_000L);
        assertEquals(3, decode.size());
    }

    @Test
    void given2hRedundantMessageWithAlarm5_whenDecoding_thenDecodesCorrectly() throws IOException, ParseException {
        final Collection<Map<String, Object>> decode = RpProductionDecoder.decode(MESSAGE_9, 1_610_607_801_000L);
        assertEquals(3, decode.size());
    }

}
